import {createRouter, createWebHashHistory} from 'vue-router'


const routes = [
    {
        path: '/',
        name: 'home',
        children:[{
            path:'/user',
            name:'user',
            component:()=>import('../views/User.vue')
        },{
            path:'/shop',
            name:'shop',
            component:()=>import('../views/Shop.vue')
        },{
            path:'/video',
            name:'video',
            component:()=>import('../views/Video.vue')
        },{
            path:'/blog',
            name:'blog',
            component:()=>import('../views/Blog.vue')
        },{
            path:'/activity',
            name:'activity',
            component:()=>import('../views/Activity.vue')
        },
        ],
        component:()=> import('@/views/HomeView.vue')
    },
    {
        path: '/login',
        name: 'login',
        component: ()=>import('@/views/Login.vue')
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})
router.beforeEach((to, from, next) => {
    if (to.path.startsWith('/login')) {
        window.localStorage.removeItem('user')
        window.localStorage.removeItem('token')
        next()
    } else {
        let admin = window.localStorage.getItem('token')
        if (admin===null) {
            next(router.push('/login'))
        } else next()
    }
})

export default router
