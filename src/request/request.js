import request from 'axios'
import {ElMessage} from "element-plus";
import router from "@/router";

const axios = request.create({
    baseURL: '/api',
    timeout: 50*1000,
    withCredentials: true,
})
axios.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';
      // 设置请求头
    config.headers.token=localStorage.getItem('token')
    return config
}, error => {
    return Promise.reject(error)
});
axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        //网络超时异常处理
        if(error.code === 'ECONNABORTED' || error.message ===   "Network Error" ||  error.message.includes("timeout")){
          ElMessage.error('请求超时，请稍后重试!')
        }
        if (error.message.includes("401")){
            ElMessage.error('登录过期，请重新登录！')
            router.push('/login')
        }

        return Promise.resolve(error.response);
    }
)
export default axios

